from django.db.models import F, DecimalField
from django.http import HttpResponse
from django.views.generic import TemplateView, ListView
from .models import Truck
from .forms import TruckForm
from django.db.models import Case, Value, When
from django.db.models.functions import Cast


class TruckListView(ListView):
    model = Truck

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = TruckForm()
        return context

    def get_queryset(self):
        qs = super().get_queryset()
        input_truck_model = self.request.GET.get('truck_model')
        if input_truck_model is not None:
            qs = qs.filter(truck_model=int(input_truck_model))

        overload_percentage_case = Case(
            When(
                current_weight__gte=F('truck_model__load_capacity'),
                then=(F('current_weight') * 1.0 / F('truck_model__load_capacity') - 1) * 100),
            default=Value(0.0),
        )
        qs = qs.annotate(overload_percentage=overload_percentage_case)
        return qs
