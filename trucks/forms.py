from django import forms
from .models import Truck, TruckModel


class TruckForm(forms.ModelForm):
    class Meta:
        model = Truck
        fields = ('truck_model',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['truck_model'].widget.is_required = False
        self.fields['truck_model'].empty_label = 'Все'
        # self.fields['truck_model'].queryset = TruckModel.objects.none()

    def get_context(self):
        ct = super().get_context()
        return ct

