from django.db import models

# Create your models here.


class TruckModel(models.Model):
    name = models.CharField(max_length=255)
    load_capacity = models.PositiveIntegerField()

    def __str__(self):
        return self.name


class Truck(models.Model):
    side_number = models.CharField(max_length=255)
    truck_model = models.ForeignKey(TruckModel, on_delete=models.CASCADE)
    current_weight = models.PositiveIntegerField()

    def __str__(self):
        return f"{self.truck_model.name} {self.side_number}"
