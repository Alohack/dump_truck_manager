from django.contrib import admin
from .models import Truck, TruckModel


@admin.register(Truck)
class TruckAdmin(admin.ModelAdmin):
    pass


@admin.register(TruckModel)
class TruckModelAdmin(admin.ModelAdmin):
    pass
